# REST UTILS Employees

The main purpose of this project is to expose the basic operations of an Employee by using RESTful and Basic Authentication

---

## Getting Started

### Prerequisites

The following applications are needed in order to run the project:


[JDK 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

[Sourcetree](https://www.sourcetreeapp.com/)

[Spring Tool Suite 4](https://spring.io/tools)

[POSTMAN](https://www.getpostman.com/)

---

### Installing


These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Start by downloading the project from bitbucket and clone it locally with these parameters (Using Sourcetree):

```
	- File -> Clone/View
	- A new screen will appear with the Clone settings. 
		Source: https://bloodhoundhusky@bitbucket.org/bloodhoundhusky/rest-utils.git
		Destination Path: (local folder)
		Name: rest-utils
		Local Folder: [Root]
	- Click on Clone button	
```	
Or if you have GIT Bash, you need to go into the folder you want that GIT takes as local Repository, then this command is equivalent to
```
git clone https://bloodhoundhusky@bitbucket.org/bloodhoundhusky/rest-utils.git	
```	

In Spring Tool Suite 4:
```
	- File -> Import...
	- Maven -> Existing Maven Projects
	- Root Directory -> Browse (navigate in your already cloned local repository)
	- In Projects Window make sure to click on rest-utils pom.xml file
	- Click on Finish
```

To run the application, simply right-click on Project (rest-utils) -> Run As... -> Spring Boot App

#### MAVEN JAR Generation

If you want to generate a jar file of your project, click on Run Configurations -> Maven Build -> right click and generate a new Configuration, then in the right panel window, add the following information:
```
Name: rest-utils jar
Main Tab
	Base directory: ${workspace_loc:/rest-utils}
	Goals: package
```	
From a command line, write:
```
mvn package 
```
from the main folder where you pom.xml resides
	
Double click on the jar file, or execute in your target folder:
```
java -jar rest-utils-0.0.1-SNAPSHOT.jar 
```

---

## Running the tests

For these examples, we need to install a RESTful Client such as POSTMAN, and verify that H2 database is running properly, run the application (right-click on Project (rest-utils) -> Run As... -> Spring Boot App) and continue in the next section

#### NOTE: 
Please be aware that the current application contains a data.sql file in src/main/resources folder. This file is required in order to insert the first set of data in the EMPLOYEE table and to create a sequence for the primary id.

### Verify that H2 is running

It is important to verify that H2 is running properly, make sure that the following link is working fine:

```
http://localhost:8080/h2-console
```

The login screen will appear for the H2 Database, make sure that you have the following settings:

```
	- Saved Settings: Generic H2 (Embedded)
	- Setting Name: Generic H2 (Embedded)
	- Driver Class: org.h2.Driver
	- JDBC URL: jdbc:h2:mem:testdb
	- User Name: sa
	- Password: 
	- Click on Connect
```
#### NOTE: password is left as blank
If you are able to see the Table EMPLOYEE in the left panel, then you have running the H2 Database properly 

---

### Swagger UI

in order to verify that Swagger is running properly, we need to validate that the following link is working fine: 

```
http://localhost:8080/swagger-ui.html
```

There, you can verify the operations, responses and models of the project

---

### Making a request on POSTMAN


#### Fetch all active Employees from Database:
```
	- Select GET from dropdown 
	- Add http://localhost:8080/employees/ in the URL Request text box
	- Click in Send
```
If you see the following information, then the test was successful:
```
[
    {
        "id": 1,
        "firstName": "Husky",
        "middleInitial": "H",
        "lastName": "Landin",
        "dateOfBirth": "2019-05-31T05:00:00.000+0000",
        "dateOfEmployment": "2019-05-31T05:00:00.000+0000"
    },
    {
        "id": 2,
        "firstName": "Eliott",
        "middleInitial": "E",
        "lastName": "Landin",
        "dateOfBirth": "2019-05-31T05:00:00.000+0000",
        "dateOfEmployment": "2019-05-31T05:00:00.000+0000"
    },
    {
        "id": 3,
        "firstName": "Iker",
        "middleInitial": "I",
        "lastName": "Landin",
        "dateOfBirth": "2019-05-31T05:00:00.000+0000",
        "dateOfEmployment": "2019-05-31T05:00:00.000+0000"
    }
]
```
####UPDATE
This method has changed to receive a querystring in the URL, this way, we will accept parameters for status, start-date and end-date of date of employment. with this, the call will be as follow:
```
http://localhost:8080/employees/?status=ACTIVE&start-date=2019-06-03T05:00:00.000+0000&end-date=2019-06-05T05:00:00.000+0000
or
http://localhost:8080/employees/?status=&start-date=2019-06-03T05:00:00.000+0000&end-date=2019-06-05T05:00:00.000+0000
or
http://localhost:8080/employees/?status=INACTIVE&start-date=&end-date=
```
The way of testing is the same as before, this time you will notice that you will retrieve all information from table (ACTIVE,INACTIVE) if no status is provided or if you use the old URL

#### Modify an Employee
```
	- Select PUT from dropdown 
	- Add http://localhost:8080/employees/ in the URL Request text box
	- In Headers tab, add the following key-value:
		* Key: Content-Type
		* Value: application/json
	- In Body tab, add the following:
		* check on raw radio
		* JSON (application/json) in the right most dropdown
		* in the Panel, type  
		{
		    "id": 1,
		    "firstName": "Fang",
		    "middleInitial": "White",
		    "lastName": "Landin",
		    "dateOfBirth": "2011-11-18T05:00:00.000+0000",
		    "dateOfEmployment": "2019-05-28T05:00:00.000+0000"
		}
		
		
	- Click on Send	(Blue Button)
```
If you see the same information in the bottom panel, then the test was successful. You can verify that the same information was correctly retrieved from Fetch All active Employees test case 

#### Retrieve an Employee by ID
```
	- Select GET from dropdown 
	- Add http://localhost:8080/employees/1 in the URL Request text box
	- In Headers tab, add the following key-value:
		* Key: Content-Type
		* Value: application/json
	- Click on Send	(Blue Button)
```
If you see the following information in the bottom panel, then the test was successful. 
```
{
    "id": 1,
    "firstName": "Fang",
    "middleInitial": "White",
    "lastName": "Landin",
    "dateOfBirth": "2011-11-18T05:00:00.000+0000",
    "dateOfEmployment": "2019-05-28T05:00:00.000+0000"
}
```
You can verify that the Employee information was correctly hidden from Fetch All active Employees test case

#### Delete (inactivate) an Employee
```
	- Select DELETE from dropdown 
	- Add http://localhost:8080/employees/1 in the URL Request text box
	- Open Authorization tab
	- Select TYPE -> Basic Auth from dropdown
	- Username: Fang
	- Password: Ab!cDE_f123#456 
	- In Headers tab, add the following key-value:
		* Key: Content-Type
		* Value: application/json
	- Click on Send	(Blue Button)
```
If you see the following information in the bottom panel, then the test was successful. 
```
{
    "id": 1,
    "firstName": "Fang",
    "middleInitial": "White",
    "lastName": "Landin",
    "dateOfBirth": "2011-11-18T05:00:00.000+0000",
    "dateOfEmployment": "2019-05-28T05:00:00.000+0000"
}
```

You can verify that the Employee information was correctly hidden from Fetch All active Employees test case

#### Create an Employee
```
	- Select POST from dropdown 
	- Add http://localhost:8080/employees/ in the URL Request text box
	- In Headers tab, add the following key-value:
		* Key: Content-Type
		* Value: application/json
	- In Body tab, add the following:
		* check on raw radio
		* JSON (application/json) in the right most dropdown
		* in the Panel, type  
		{
		    "firstName": "Kary",
		    "middleInitial": "K",
		    "lastName": "Landin",
		    "dateOfBirth": "2018-11-12T05:00:00.000+0000",
		    "dateOfEmployment": "2019-05-28T05:00:00.000+0000"
		}
		
		
	- Click on Send	(Blue Button)
```
If you see the following information in the bottom panel, then the test was successful. 
```
{
    "id": 5,
    "firstName": "Kary",
    "middleInitial": "K",
    "lastName": "Landin",
    "dateOfBirth": "2018-11-12T05:00:00.000+0000",
    "dateOfEmployment": "2019-05-28T05:00:00.000+0000"
}
```
You can verify that the Employee information was correctly appended from Fetch All active Employees test case

#### Create an Employee (Batch)
```
	- Select POST from dropdown 
	- Add http://localhost:8080/employees/ in the URL Request text box
	- In Headers tab, add the following key-value:
		* Key: Content-Type
		* Value: application/json
	- In Body tab, add the following:
		* check on raw radio
		* JSON (application/json) in the right most dropdown
		* in the Panel, type  
		[
		    {
		        "firstName": "Jacobo",
		        "middleInitial": "J",
		        "lastName": "Landin",
		        "dateOfBirth": "2019-04-29T05:00:00.000+0000",
		        "dateOfEmployment": "2019-04-29T05:00:00.000+0000"
		    },
		    {
		        "firstName": "Naty",
		        "middleInitial": "N",
		        "lastName": "Ramos",
		        "dateOfBirth": "2019-03-29T05:00:00.000+0000",
		        "dateOfEmployment": "2019-03-29T05:00:00.000+0000"
		    },
		    {
		        "firstName": "Ruby",
		        "middleInitial": "R",
		        "lastName": "Landin",
		        "dateOfBirth": "2019-02-28T05:00:00.000+0000",
		        "dateOfEmployment": "2019-02-28T05:00:00.000+0000"
		    }
		]
		
		
	- Click on Send	(Blue Button)
```
If you see the following information in the bottom panel, then the test was successful. 
```
[
    {
        "id": 6,
        "firstName": "Jacobo",
        "middleInitial": "J",
        "lastName": "Landin",
        "dateOfBirth": "2019-04-29T05:00:00.000+0000",
        "dateOfEmployment": "2019-04-29T05:00:00.000+0000"
    },
    {
        "id": 7,
        "firstName": "Naty",
        "middleInitial": "N",
        "lastName": "Ramos",
        "dateOfBirth": "2019-03-29T05:00:00.000+0000",
        "dateOfEmployment": "2019-03-29T05:00:00.000+0000"
    },
    {
        "id": 8,
        "firstName": "Ruby",
        "middleInitial": "R",
        "lastName": "Landin",
        "dateOfBirth": "2019-02-28T05:00:00.000+0000",
        "dateOfEmployment": "2019-02-28T05:00:00.000+0000"
    }
]
```
You can verify that the Employee information was correctly appended from Fetch All active Employees test case

---

## Deployment notes
The main structure of the project is as follow:
```
	rest-utils
	|_.mvn/wrapper		
	|_src	
	|_.gitignore
	|_README.md    <--- You are reading this file
	|_mvnw
	|_mvnw.cmd
	|_pom.xml      <--- Main pom file
```
Inside src folder, you will find out the following structure:
```
	main
	|_java/com/crokenworks/test/restutils
	|_resources
```	
Let's verify the content of resources folder first, inside you will have:
```
	application.properties		<--- Here you will find all resources used in the project
	data.sql					<--- You can configure some scripts here, basically H2 makes 
									 everything for you, but there are some other configurations 
									 which needs to be declared first, like sequences and
									 insert statements 
```
Now, let's take a look inside java/com/crokenworks/test/restutils structure:
```
	|_backend					<--- backend package where dto, entity, repository and service logics are contained
	|_config					<--- package containing extra configurations such as security and swagger
	|_controller				<--- controllers of project
	|_exceptions				<--- custom exceptions
	|_utils						<--- this package contains Utility classes, such as Gson handlers and Status of Employee
	|_RestUtilsApplication.java <--- Main class, it all begins here
```

---

## Built With

* [Spring Boot](https://start.spring.io/) - Bootstrap your application
* [Maven](https://maven.apache.org/) - Dependency Management

---


## Authors

* **Guillermo Landin** - *Initial work* 

---

## License

This project is licensed under the MIT License 

---

## Acknowledgments

Udemy courses, experience gained with hard work, and my family
