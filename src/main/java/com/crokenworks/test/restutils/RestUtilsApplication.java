package com.crokenworks.test.restutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestUtilsApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestUtilsApplication.class, args);
	}

}
