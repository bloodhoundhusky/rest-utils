package com.crokenworks.test.restutils.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.crokenworks.test.restutils.backend.entity.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>, JpaSpecificationExecutor<Employee>{

//	@Query("SELECT e FROM Employee e where e.status = :status")
//	List<Employee> findAllByActive(@Param(value = "status") String status);
}
