package com.crokenworks.test.restutils.backend.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.crokenworks.test.restutils.backend.entity.Employee;

public class EmployeeSpecification implements Specification<Employee>{
	
	private static final long serialVersionUID = 2647534456461198919L;
	
	private Employee employee;
	private Date startDate, endDate;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public EmployeeSpecification(Employee employee, String startDate, String endDate){
		super();
		this.employee = employee;

		try {
			this.startDate = startDate != null && !startDate.equals("") ? sdf.parse(startDate) : null;
		} catch (ParseException e) {
			this.startDate = null;
		}
		try {
			this.endDate = endDate != null && !endDate.equals("") ? sdf.parse(endDate) : null;
		} catch (ParseException e) {
			this.endDate = null;
		}
	}

	
	@Override
	public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

		Predicate p = cb.disjunction();
		boolean param = false;
		
		if(employee.getStatus() != null && !employee.getStatus().equals("")) {
			p.getExpressions().add(cb.equal(root.get("status"), employee.getStatus()));
			param = true;
		}
		
		if(employee.getDateOfEmployment() != null && startDate != null && endDate != null) {
			p.getExpressions().add(
				cb.and(
					cb.greaterThanOrEqualTo(root.get("dateOfEmployment"), startDate),
					cb.lessThan(root.get("dateOfEmployment"), endDate)
				)
			);
			param = true;
		}

		return !param ? null : p;
	}

}
