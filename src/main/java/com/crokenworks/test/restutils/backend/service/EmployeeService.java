package com.crokenworks.test.restutils.backend.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.crokenworks.test.restutils.backend.dto.EmployeeDTO;

@Service("employeeService")
public interface EmployeeService {
	public List<EmployeeDTO> findAll(String status, String startDate, String endDate);
	public List<EmployeeDTO> findAllActive();
	public EmployeeDTO createEmployee(EmployeeDTO employee);
	public EmployeeDTO updateEmployee(EmployeeDTO employee);
	public EmployeeDTO getEmployeeById(Integer id);
	public EmployeeDTO inactivateEmployee(Integer id);
	public List<EmployeeDTO> createEmployeeByBatch(List<EmployeeDTO> employees);
}
