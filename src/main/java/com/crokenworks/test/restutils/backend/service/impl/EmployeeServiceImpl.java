package com.crokenworks.test.restutils.backend.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.crokenworks.test.restutils.backend.dto.EmployeeDTO;
import com.crokenworks.test.restutils.backend.entity.Employee;
import com.crokenworks.test.restutils.backend.repository.EmployeeRepository;
import com.crokenworks.test.restutils.backend.repository.EmployeeSpecification;
import com.crokenworks.test.restutils.backend.service.EmployeeService;
import com.crokenworks.test.restutils.exceptions.EmployeeNotFoundException;
import com.crokenworks.test.restutils.utils.GsonDTOUtils;
import com.crokenworks.test.restutils.utils.StatusUtil.STATUS_ENUM;

import ch.qos.logback.core.status.Status;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Override
	@Transactional
	public List<EmployeeDTO> findAllActive() {
		//List<Employee> employees = employeeRepository.findAllByActive(StatusUtil.getStatus(STATUS_ENUM.ACTIVE));
		
		Employee employee = new Employee();
		employee.setStatus(STATUS_ENUM.ACTIVE.toString());

		EmployeeSpecification spec = new EmployeeSpecification(employee, null, null);
		
		List<Employee> employees = employeeRepository.findAll(spec);
		return GsonDTOUtils.getListAsDTO(employees);
	}

	@Override
	@Transactional
	public EmployeeDTO createEmployee(EmployeeDTO employee) {
		Employee employeeEntity = new Employee();
		BeanUtils.copyProperties(employee, employeeEntity);
		BeanUtils.copyProperties(employeeRepository.save(employeeEntity), employee);
		return employee;
	}

	@Override
	@Transactional
	public EmployeeDTO updateEmployee(EmployeeDTO employee) {
		Optional<Employee> foundEmployee = employeeRepository.findById(employee.getId());
		if(!foundEmployee.isPresent()) {
			throw new EmployeeNotFoundException("id-" + employee.getId() + ", not found!");
		}
		if(!foundEmployee.get().getStatus().equals(STATUS_ENUM.ACTIVE.toString())) {
			throw new EmployeeNotFoundException("id-" + employee.getId() + ", is INACTIVE!");
		}
		//employee.setStatus(StatusUtil.getStatus(STATUS_ENUM.ACTIVE)); is this needed?
		Employee employeeEntity = new Employee();
		BeanUtils.copyProperties(employee, employeeEntity);
		BeanUtils.copyProperties(employeeRepository.save(employeeEntity), employee);
		
		return employee;
	}

	@Override
	@Transactional
	public EmployeeDTO getEmployeeById(Integer id) {
		Optional<Employee> foundEmployee = employeeRepository.findById(id);
		if(!foundEmployee.isPresent()) {
			throw new EmployeeNotFoundException("id-" + id + ", not found!");
		}
		
		if(foundEmployee.get().getStatus().equals(STATUS_ENUM.INACTIVE.toString())){
			throw new EmployeeNotFoundException("employee with id " + id + " is currently inactive!");
		}
		EmployeeDTO employeeDTO = new EmployeeDTO();
		BeanUtils.copyProperties(foundEmployee.get(), employeeDTO);
		
		return employeeDTO;
	}

	@Override
	@Transactional
	public EmployeeDTO inactivateEmployee(Integer id) {
		Optional<Employee> foundEmployee = employeeRepository.findById(id);
		if(!foundEmployee.isPresent()) {
			throw new EmployeeNotFoundException("id-" + id + ", not found!");
		}
		
		foundEmployee.get().setStatus(STATUS_ENUM.INACTIVE.toString());
		
		EmployeeDTO employeeDTO = new EmployeeDTO();
		BeanUtils.copyProperties(employeeRepository.save(foundEmployee.get()), employeeDTO);
		
		return employeeDTO;
	}

	@Override
	@Transactional
	public List<EmployeeDTO> createEmployeeByBatch(List<EmployeeDTO> employeesDTO) {
		List<Employee> employeeEntityList = GsonDTOUtils.getListAsEntities(employeesDTO); 
		employeeEntityList = employeeRepository.saveAll(employeeEntityList);
		
		return GsonDTOUtils.getListAsDTO(employeeEntityList);
	}

	public EmployeeRepository getEmployeeRepository() {
		return employeeRepository;
	}

	public void setEmployeeRepository(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	@Override
	@Transactional
	public List<EmployeeDTO> findAll(String status, String startDate, String endDate) {

		Employee employee = new Employee();
		if(status == null || status.equals(""))
			employee.setStatus(STATUS_ENUM.ACTIVE.toString());
		else
			employee.setStatus(status);

		EmployeeSpecification spec = new EmployeeSpecification(employee, startDate, endDate);

		return GsonDTOUtils.getListAsDTO(employeeRepository.findAll(spec));
	}
	
}
