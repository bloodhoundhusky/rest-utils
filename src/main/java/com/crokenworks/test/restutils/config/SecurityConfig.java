package com.crokenworks.test.restutils.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		
		
		httpSecurity.headers().frameOptions().disable(); //prevents header to be sent as part of response
		httpSecurity.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) //never create an HttpSession, it will never use to obtain the SecurityContext
		.and()
		.authorizeRequests()
		.antMatchers("/h2-console/**").permitAll()
		.antMatchers("/ui-swagger.html").permitAll()
		.antMatchers("/swagger-resources/**").permitAll()
		.antMatchers("/v2/api-docs").permitAll()
		.antMatchers(HttpMethod.POST, "/employees/**").permitAll()
		.antMatchers(HttpMethod.PUT, "/employees/**").permitAll()
		.antMatchers(HttpMethod.GET, "/employees/**").permitAll()
		.antMatchers(HttpMethod.DELETE, "/employees/**").hasRole("ADMIN")
		;
		httpSecurity.csrf().disable(); //Cross-Site Request Forgery, if not disabled, no H2 console
		httpSecurity.httpBasic(); //Basic Authorization/Authentication
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder authentication) throws Exception {
		authentication.
		inMemoryAuthentication().
		withUser("Fang").
		password(passwordEncoder().encode("Ab!cDE_f123#456")).
		roles("ADMIN");
	}
	
	@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
