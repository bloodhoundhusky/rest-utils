package com.crokenworks.test.restutils.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.crokenworks.test.restutils.backend.dto.EmployeeDTO;
import com.crokenworks.test.restutils.backend.service.EmployeeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/employees")
public class EmployeeRestController {

	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/")
	@ApiOperation(value = "Fetch all Employees which are ACTIVE. Returns 200 OK operation if success", responseContainer = "List", response = EmployeeDTO.class)
	public ResponseEntity<?> getAllEmployeesActive(
			@RequestParam(required = false, name = "status") String status, 
			@RequestParam(required = false, name = "start-date") String startDate,
			@RequestParam(required = false, name = "end-date")String endDate){

		return new ResponseEntity<>(employeeService.findAll(status, startDate, endDate), HttpStatus.OK);
		
		//return new ResponseEntity<>(employeeService.findAllActive(), HttpStatus.OK);
	}
	
	@PostMapping("/")
	@ApiOperation(value = "Creates all details related to the Employee. Returns 200 OK operation if success", response = EmployeeDTO.class)
	public ResponseEntity<?> createEmployee(@Valid @RequestBody EmployeeDTO employee) {
		return new ResponseEntity<>(employeeService.createEmployee(employee), HttpStatus.OK);
	}
	
	@PutMapping("/")
	@ApiOperation(value = "Updates the Employee information. Returns 200 OK operation if success", response = EmployeeDTO.class)
	public ResponseEntity<?> modifyEmployee(@Valid @RequestBody EmployeeDTO employee) {
		return new ResponseEntity<>(employeeService.updateEmployee(employee), HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	@ApiOperation(value = "Inactivates the Employeed based on this id. Returns 200 OK operation if success", response = EmployeeDTO.class)
	@ApiParam(value = "Mandatory value indicating the ID of user to INACTIVATE", required = true)
	public ResponseEntity<?> inactivateEmployee(@PathVariable Integer id) {
		
		return new ResponseEntity<>(employeeService.inactivateEmployee(id), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	@ApiOperation(value = "Returns Employee information based on this Id. Returns 200 OK operation if success", response = EmployeeDTO.class)
	public ResponseEntity<?> getEmployeeById(@PathVariable Integer id){
		return new ResponseEntity<>(employeeService.getEmployeeById(id), HttpStatus.OK);
	}
	
	@PostMapping("/create-batch")
	@ApiOperation(value = "Receives a list of Employees to be created. Returns 200 OK operation if success", responseContainer = "List", response = EmployeeDTO.class)
	@ApiParam(value = "List of Employees in json format to be Created", required = true)
	public ResponseEntity<?> createEmployeesByBatch(@Valid @RequestBody List<EmployeeDTO> employees) {
		return new ResponseEntity<>(employeeService.createEmployeeByBatch(employees), HttpStatus.OK);
	}
}
