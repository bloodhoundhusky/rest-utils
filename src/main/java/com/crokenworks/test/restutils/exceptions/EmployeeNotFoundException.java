package com.crokenworks.test.restutils.exceptions;

import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.annotations.ApiModel;

import org.springframework.http.HttpStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@ApiModel(description = "Basic class of Exception when a RuntimeException has been found. (EmployeeNotFound)")
public class EmployeeNotFoundException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public EmployeeNotFoundException(String message) {
		super(message);
	}
}
