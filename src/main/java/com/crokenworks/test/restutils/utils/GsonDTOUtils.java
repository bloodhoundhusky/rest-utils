package com.crokenworks.test.restutils.utils;

import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

import com.crokenworks.test.restutils.backend.dto.EmployeeDTO;
import com.crokenworks.test.restutils.backend.entity.Employee;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

@ApiModel(description = "Utility class which uses Gson as core. It handles json notation conversion for Employee Entity to Employee DTO and viceversa.")
public class GsonDTOUtils {
	
	@ApiOperation(value = "Method which receives a List of Employee and returns a JSon String",
		    response = String.class)
	public static String getList(List<Employee> employees){
		Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
		String json = gson.toJson(employees);
		return json;
	}

	@ApiOperation(value = "Method which receives a List of EmployeeDTO and returns a List of Employee",
		    response = Employee.class,
		    responseContainer = "List")
	public static List<Employee> getListAsEntities(List<EmployeeDTO> employees) {
		Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
		String json = gson.toJson(employees);
		Employee[] employeeEntArray = gson.fromJson(json, Employee[].class);
		
		return Arrays.asList(employeeEntArray);
	}
	
	@ApiOperation(value = "Method which receives a List of EmployeeDTO and returns a List of Employee",
		    response = Employee.class,
		    responseContainer = "List")
	public static List<EmployeeDTO> getListAsDTO(List<Employee> employees) {
		Gson gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.TRANSIENT).create();
		String json = gson.toJson(employees);
		EmployeeDTO[] employeeDTOArray = gson.fromJson(json, EmployeeDTO[].class);
		
		return Arrays.asList(employeeDTOArray);
	}
}
