package com.crokenworks.test.restutils.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;

@ApiModel(description = "Utility class which includes Status of Employee: ACTIVE/INACTIVE")
public class StatusUtil {
	public static enum STATUS_ENUM {
		ACTIVE, INACTIVE
	}
	
	@ApiOperation(value = "Method which converts the given STATUS_ENUM to the desired status as String",
		    response = String.class)
	public static String getStatus(STATUS_ENUM status) {
		switch(status) {
			case ACTIVE: return "ACTIVE";
			case INACTIVE: return "INACTIVE";
			default: return "ACTIVE";
		}
	}
}
