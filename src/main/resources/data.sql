DROP SEQUENCE EMPLOYEE_ID_SEQ;
CREATE SEQUENCE EMPLOYEE_ID_SEQ
  MINVALUE 1
  MAXVALUE 9999999999999999
  START WITH 1
  INCREMENT BY 1
  CACHE 1;
  
insert into employee values(EMPLOYEE_ID_SEQ.nextval, sysdate(), sysdate(), 'Husky', 'Landin', 'H', 'ACTIVE');
insert into employee values(EMPLOYEE_ID_SEQ.nextval, sysdate(), sysdate(), 'Eliott', 'Landin', 'E', 'ACTIVE');
insert into employee values(EMPLOYEE_ID_SEQ.nextval, sysdate(), sysdate(), 'Iker', 'Landin', 'I', 'ACTIVE');
insert into employee values(EMPLOYEE_ID_SEQ.nextval, sysdate(), sysdate(), 'Guillermo', 'Landin', 'G', 'INACTIVE');